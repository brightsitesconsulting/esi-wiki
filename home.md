<!-- TITLE: ESI Wiki -->
<!-- SUBTITLE: Documentation for the ESI group of websites -->

# Overview

Home Page



# How to Use this Wiki
Documentation for wiki.js can be found at https://docs.requarks.io/wiki.  Below I am only going to document some of the features used frequently and the ones that are needed for this wiki.


### Adding Navigation Items

To add navigation item create a page e.g. **Developement** then add some content to that page then while on that level create another page call it **indy** for example, once that done you will then have a structure like so: 

`Developement->indy`

### Removing Directories 

First make sure to remove any pages under that directory from UI then to remove directories from navigation you would need to ssh into the server and got to:

`/var/www/esi-wiki/repo`

and do:

`sudo rm <directory_name>`

For the changes to take place you would need to restart wiki by typing:

`node wiki restart`


# Code Highlight
Here is some example code higlight for different language.

**Php Code**


```php
<?php
//Code goes here.

```

**Javascript Code**

```javascript
const testConst = true;
let testLet = true;
var testVar = true;
```

While on page edit click on < > inline code format or </> for formating a block of code.

