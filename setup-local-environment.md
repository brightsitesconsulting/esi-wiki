<!-- TITLE: Setup Local Environment -->
<!-- SUBTITLE: A quick summary of Setup Local Environment -->

# Setup local environment
## Requirements
* Virtualbox
* Ansible
* Vagrant
* DrupalVM
* MySQL
> MySQL inside DrupalVM is not suggested because the data size keep expanding dramatically. It can be a nightmare if it reach the maximum size of virtual machine.

## Configurations
### DrupalVM
All required configurations are at [drupalvm-config](https://bitbucket.org/brightsitesconsulting/ines/src/master/drupalvm-config/) in [ines](https://bitbucket.org/brightsitesconsulting/ines) repository. You should create symbolic links of all these files / folder in your DrupalVM root directory.
### MySQL
> If you use MAMP, you need to edit **my.cnf** with the following settings.

```ini
[mysqld]
innodb_file_format=barracuda
innodb_file_per_table=1
innodb_large_prefix=1
init_connect='SET collation_connection = utf8mb4_unicode_ci'
init_connect='SET NAMES utf8mb4'
character-set-server=utf8mb4
collation-server=utf8mb4_unicode_ci
 
max_allowed_packet = 1073741824
```

